""" Base class for passwords and secrets management via Vault """

import requests
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    HTTPRequestFailedException, InvalidValueException
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.utils.constants import VAULT_PORT_CONFIG, \
    VAULT_IP_CONFIG, VAULT_CONFIG, VAULT_TOKEN_CONFIG, VAULT_RESPONSE_DATA_KEY, \
    VAULT_TOKEN_HEADER, VAULT_IP_SUFFIX_KV, VAULT_NAMESPACE_XPRESSO, \
    HTTP_URL_PREFIX

__all__ = ['VaultManager']
__author__ = 'Sahil Malav'


class VaultManager:

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH):
        config = XprConfigParser(config_path)
        self.token = config[VAULT_CONFIG][VAULT_TOKEN_CONFIG]
        self.server_ip = config[VAULT_CONFIG][VAULT_IP_CONFIG]
        self.port = config[VAULT_CONFIG][VAULT_PORT_CONFIG]
        self.logger = XprLogger()

    @staticmethod
    def construct_vault_url_and_header(namespace, server_ip, port, token):
        """
        Constructs IP and header to send requests to Vault
        Args:
            namespace: namespace of the secret
            server_ip: IP where Vault is hosted
            port: Vault port
            token: root token

        Returns: url, header

        """
        return f"{server_ip}:{port}{VAULT_IP_SUFFIX_KV}{namespace}", \
               {VAULT_TOKEN_HEADER: token}

    def fetch_request_parameters(self, namespace, port, server_ip, token):
        """ fetches parameters required for sending api request to Vault """
        if not namespace:
            namespace = VAULT_NAMESPACE_XPRESSO
        if not server_ip:
            server_ip = self.server_ip
        if HTTP_URL_PREFIX not in server_ip:
            server_ip = f'{HTTP_URL_PREFIX}{server_ip}'
        if not port:
            port = self.port
        if not token:
            token = self.token
        url, header = self.construct_vault_url_and_header(
            namespace=namespace, server_ip=server_ip, port=port, token=token)
        return url, header

    def get_secrets(self, namespace=None, server_ip=None, port=None,
                    token=None):
        """
        Gets secrets in a particular namespace (xpresso namespace by default)
        Args:
            namespace: namespace of the secret
            server_ip: IP where Vault is hosted
            port: Vault port
            token: Vault root token

        Returns(dict): Secrets in the requested namespace

        """
        url, header = self.fetch_request_parameters(
            namespace=namespace, server_ip=server_ip, port=port, token=token)
        response = requests.get(url=url, headers=header).json()
        if VAULT_RESPONSE_DATA_KEY not in response:
            raise HTTPRequestFailedException(
                f'Failed to fetch secrets from Vault.\n{response}')
        return response[VAULT_RESPONSE_DATA_KEY]

    def get_existing_data(self, namespace, port, server_ip, token):
        """ gets existing secrets in namespace, if any """
        url, header = self.fetch_request_parameters(
            namespace=namespace, server_ip=server_ip, port=port, token=token)
        print(header)
        print(url)
        response = requests.get(url=url, headers=header).json()
        if VAULT_RESPONSE_DATA_KEY not in response:
            existing_data = {}
        else:
            existing_data = response[VAULT_RESPONSE_DATA_KEY]
        return existing_data, header, url

    def add_or_modify_secrets(self, secrets, namespace=None, server_ip=None,
                              port=None, token=None):
        """
        Adds secrets to a given namespace in Vault. Also works for modification.
        Args:
            secrets(dict): key-value pair of secret(s) to be added
            namespace: namespace of the secret
            server_ip: IP where Vault is hosted
            port: Vault port
            token: Vault root token

        Returns: Nothing

        """
        if not secrets:
            raise InvalidValueException('No secrets specified for adding.')
        existing_data, header, url = self.get_existing_data(namespace, port,
                                                            server_ip, token)
        for key in secrets:
            if key in existing_data:
                del existing_data[key]
        combined_data = {**existing_data, **secrets}
        add_response = requests.post(url=url, headers=header, data=combined_data)
        if add_response.text:
            raise HTTPRequestFailedException(
                f'Failed to add secret(s) to Vault.\n{add_response.text}')
        print('Secrets added successfully!')

    def delete_secrets(self, secrets, namespace=None, server_ip=None,
                       port=None, token=None):
        """
        Deletes given secret(s) from a given namespace in Vault
        Args:
            secrets(list): list of secret(s) to be deleted
            namespace: namespace of the secret
            server_ip: IP where Vault is hosted
            port: Vault port
            token: Vault root token

        Returns: Nothing

        """
        if not secrets:
            raise InvalidValueException('No secrets specified for deletion.')
        existing_data, header, url = self.get_existing_data(
            namespace, port, server_ip, token)
        if not existing_data:
            raise HTTPRequestFailedException(
                'The provided namespace does not contain any secrets.')
        if set(existing_data.keys()).issubset(set(secrets)):
            raise InvalidValueException(
                'Cannot delete all the secrets in a namespace.')
        for secret in secrets:
            existing_data.pop(secret, None)
        add_response = requests.post(url=url, headers=header, data=existing_data)
        if add_response.text:
            raise HTTPRequestFailedException(
                f'Failed to delete secret(s) from Vault.\n{add_response.text}')
        print('Secrets deleted successfully!')




